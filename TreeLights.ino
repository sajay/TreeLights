/********************************************************************
 * Simon Jay
 * 
 * This program displays Christmas tree lights!
 * Uses 3 wires and 3 pins for display lights
 * 
 * Uses 2 buttons and 2 pins for input
 * 
 *    This uses the fact that LEDs function only when the current 
 * flows one way, and doesn't permit movement in the other. Meaning
 * Flow from BLUE -> RED is not the same as Red -> Blue, allowing
 * our 4 colors on only 3 wires. (Up to 6 could be used). We use
 * HIGH for when we don't want to allow movement on a wire as well.
 * 
 *    Mode is set by holding both of the buttons (closing circuit)
 * on pins SIG0 and SIG1 to the ground. Lights then all turn on,
 * giving the user 2 second to input a new mode, based on which
 * of the two buttons are held.
 ********************************************************************/

//How long should we pause to emit a single color
const byte DELAY_TIME = 1;
//Refresh rate
int ROUNDS = 255;
//Style our lights will blink/display
byte mode = 1;

//Input pins
const byte SIG0 = 0;
const byte SIG1 = 1;

//Output pins
const byte BLUE_WIRE = 2;
const byte RED_WIRE = 3;
const byte WHITE_WIRE = 4;

//Frequency of each color
//The int results in approx. 18-sqrt(x) light seen, so 16 is very dim, 1 is steady on.
//0 however is off by axiom.
byte blue = 16;
byte green = 1;
byte red = 0;
byte yellow = 1;

void setup(){
  pinMode(SIG0, INPUT_PULLUP);
  pinMode(SIG1, INPUT_PULLUP);
  pinMode(BLUE_WIRE, OUTPUT);
  pinMode(RED_WIRE, OUTPUT);
  pinMode(WHITE_WIRE, OUTPUT);
  digitalWrite(BLUE_WIRE, LOW);
  digitalWrite(RED_WIRE, LOW);
  digitalWrite(WHITE_WIRE, LOW);
  randomSeed(analogRead(0));
  setBinRand();
}
void loop(){
  while(true){
    //Get new program
    if (!digitalRead(SIG0) || !digitalRead(SIG1)){
      digitalWrite(BLUE_WIRE, LOW);
      digitalWrite(RED_WIRE, HIGH);
      digitalWrite(WHITE_WIRE, LOW);
      delay(2000);
      digitalWrite(BLUE_WIRE, LOW);
      digitalWrite(RED_WIRE, LOW);
      digitalWrite(WHITE_WIRE, LOW);
      
      if (!digitalRead(SIG0) && !digitalRead(SIG1)){
        setSteady();
      }
      if (!digitalRead(SIG0) && digitalRead(SIG1)){
        setBinRand();
      }
      else if (digitalRead(SIG0) && !digitalRead(SIG1)){
        setSlowTrail();
      }
      else if (digitalRead(SIG0) && digitalRead(SIG1)){
        setTrail();
      }
      delay(2000);
    }
    //parse by mode
    if (mode == 1){
      parseBinRand();
    }
    else if (mode == 2){
      parseSlowTrail();
    }
    else if (mode == 3){
      parseTrail();
    }
    //run display
    disp(ROUNDS);
  }  
}
void setBinRand(){
  blue = 1;
  green = 1;
  red = 1;
  yellow = 1;
  mode = 1;
  ROUNDS = 1024;
}
void parseBinRand(){
  blue = random(2);
  green = random(2);
  red = random(2);
  yellow = random(2);
}
void setSteady(){
  blue = 1;
  green = 1;
  red = 1;
  yellow = 1;
  mode = 0;
  ROUNDS = 255;
}
void parseSteady(){};
void setSlowTrail(){
  blue = 1;
  green = 7;
  red = 13;
  yellow = 19;
  mode = 2;
  ROUNDS = 2048;  
}
void parseSlowTrail(){
  blue = ((blue+1)%24)+1;
  green = ((green+1)%24)+1;
  red = ((red+1)%24)+1;
  yellow = ((yellow+1)%24)+1;
}
void setTrail(){
  blue = 1;
  green = 7;
  red = 13;
  yellow = 19;
  mode = 3;
  ROUNDS = 255;  
}
void parseTrail(){
  blue = ((blue+5)%24)+1;
  green = ((green+5)%24)+1;
  red = ((red+5)%24)+1;
  yellow = ((yellow+5)%24)+1;
}
void disp(int rounds){
  for (int i = rounds; i; i--){
    if (!(i % blue) && blue < 10){
      digitalWrite(RED_WIRE, LOW);
      digitalWrite(WHITE_WIRE, LOW);
      digitalWrite(BLUE_WIRE, HIGH);
      delay(DELAY_TIME);
    }
    if (!(i % green) && green < 10){
      digitalWrite(BLUE_WIRE, LOW);
      digitalWrite(RED_WIRE, HIGH);
      digitalWrite(WHITE_WIRE, HIGH);
      delay(DELAY_TIME);
    }
    if (!(i % red) && red < 10){
      digitalWrite(RED_WIRE, LOW);
      digitalWrite(BLUE_WIRE, LOW);
      digitalWrite(WHITE_WIRE, HIGH);
      delay(DELAY_TIME);
    }
    if (!(i % yellow) && yellow < 10){
      digitalWrite(WHITE_WIRE, LOW);
      digitalWrite(RED_WIRE, HIGH);
      digitalWrite(BLUE_WIRE, HIGH);
      delay(DELAY_TIME);
    }
  }
}
